var express = require('express');
var router = express.Router();

router.get('/diagrams', function(req, res, next) {
  res.render('diagrams', { title: 'RFID' });
});

router.get('/add-place', function(req, res, next) {
  res.render('add-place', { title: 'RFID' });
});

router.get('/add-product', function(req, res, next) {
  res.render('add-product', { title: 'RFID' });
});

router.get('/add-rfid', function(req, res, next) {
  res.render('add-rfid', { title: 'RFID' });
});

router.get('/', function(req, res, next) {
  res.render('index', { title: 'RFID' });
});

module.exports = router;
